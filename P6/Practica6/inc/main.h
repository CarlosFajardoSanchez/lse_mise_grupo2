#ifndef __MAIN_H
#define __MAIN_H

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"
#include "stm32f411e_discovery.h"

/*
 * Definiciones
 */

// Boton
#define PRESSED_BUTTON_NONE 0
#define PRESSED_BUTTON_USER	1

/* Handler Uart */

/*
 * Funciones
 */

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin);
void InitializeTimer();
void InitializePWMChannel();
void leds_PWM();
void leds_normal();
void Inicializacion();

// USB y Uart
void UartInicializacion();
uint32_t Demo_USBConfig(void);
void SystemClock_Config(void);
void Error_Handler(void);



#endif /* __MAIN_H */
