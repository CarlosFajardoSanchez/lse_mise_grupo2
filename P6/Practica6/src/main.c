/**
  ******************************************************************************
  * @file    main.c
  * @author  Ac6
  * @version V1.0
  * @date    01-December-2013
  * @brief   Default main function.
  ******************************************************************************
*/

#include <stm32f4xx_hal.h>
#include "stm32f4xx.h"
#include "stm32f411e_discovery.h"
#include "stm32f411e_discovery_accelerometer.h"

#include "usbd_cdc.h"
#include "usbd_cdc_if.h"
#include "usbd_desc.h"

/* Handler Uart */
UART_HandleTypeDef myUart;
__IO ITStatus UartReady = RESET;

// Varibales PWM
#define PERIODO     1000
#define PWM1        333
#define PWM2        500
#define PWM3        666

#define PRESSED_BUTTON_NONE 0
#define PRESSED_BUTTON_USER	1

#define RCV_BUFFER_SIZE 	2

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin);
void InitializeTimer();
void InitializePWMChannel();
void leds_PWM();
void leds_normal();
void Inicializacion();

// USB y Uart
void UartInicializacion();
uint32_t Demo_USBConfig(void);
void SystemClock_Config(void);
void Error_Handler(void);

// Reloj Tim4
TIM_HandleTypeDef htim4;
TIM_OC_InitTypeDef sConfigTim4;

/*
 * Variables
 */
// Estado de la FSM y flag para el boton
uint8_t state = 0;
uint8_t flag = 0;

// Flag enable Uart
uint8_t flagUart = 1;

// Constantes de estado 1 (tiempo led)
int t_led4 = 250;
int t_led5 = 500;
int t_led6 = 1000;

// ERROR FLAG
int16_t error = 0;

// Buffer send
uint8_t rcvBuffer[RCV_BUFFER_SIZE];
char bufferSend[64];
int len;


int main(void)
{
	Inicializacion();
	InitializeTimer();
	InitializePWMChannel();

	SystemClock_Config();
	BSP_ACCELERO_Init();
	UartInicializacion();
	Demo_USBConfig();

	while(1){
/*
		while (UartReady != SET)
		{
		}

		UartReady = RESET;
*/
		if(HAL_UART_Receive_IT(&myUart, (uint8_t *)rcvBuffer, RCV_BUFFER_SIZE) != HAL_OK)
		{
			//Error_Handler();
		}

/*
		while (UartReady != SET)
		{
		}
*/
		UartReady = RESET;

		if (rcvBuffer[0] == '0') {
			if (flagUart == 1) {
				sprintf(bufferSend,"NO estamos transmitiendo, pulsar 1 para transmitir\r\n");
				len = strlen(bufferSend);

				if(HAL_UART_Transmit(&myUart, (uint8_t*)bufferSend, len, 1000)!= HAL_OK)
				{
					error = 1;
					Error_Handler();
					error = 0;
				}
			}
			flagUart = 0;
		} else if (rcvBuffer[0] == '1') {
			if (flagUart == 0) {
				sprintf(bufferSend,"Estamos transmitiendo, pulsar 0 para no transmitir\r\n");
				len = strlen(bufferSend);

				if(HAL_UART_Transmit(&myUart, (uint8_t*)bufferSend, len, 1000)!= HAL_OK)
				{
					error = 1;
					Error_Handler();
					error = 0;
				}
			}
			flagUart = 1;
		}

		if (state == 0) {
			leds_normal();
			BSP_LED_Off(LED3);
		}
		else if (state == 1){
			leds_PWM();
			BSP_LED_On(LED3);
		}
	}
}


void SystemClock_Config(void)
{
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_OscInitTypeDef RCC_OscInitStruct;

  /* Enable Power Control clock */
  __HAL_RCC_PWR_CLK_ENABLE();

  /* The voltage scaling allows optimizing the power consumption when the device is
     clocked below the maximum system frequency, to update the voltage scaling value
     regarding system frequency refer to product datasheet.  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE2);

  /* Enable HSE Oscillator and activate PLL with HSE as source */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 336;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV4;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) {
	  Error_Handler();
  }

  /* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2
     clocks dividers */
  RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;
  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK) {
	  Error_Handler();
  }
}

uint32_t Demo_USBConfig(void)
{
  /* Init Device Library */
  USBD_Init(&hUSBDDevice, &CDC_Desc, 0);

  /* Add Supported Class */
  USBD_RegisterClass(&hUSBDDevice, USBD_CDC_CLASS);

  /* Register user interface */
  USBD_CDC_RegisterInterface(&hUSBDDevice, &USBD_CDC_LSE_fops);

  /* Start Device Process */
  USBD_Start(&hUSBDDevice);

  /* Prepare for rx */
  //USBD_CDC_ReceivePacket(&hUSBDDevice);

  return 0;
}


void UartInicializacion() {
	myUart.Instance          = USART2;

	myUart.Init.BaudRate     = 115200;
	myUart.Init.WordLength   = UART_WORDLENGTH_8B;
	myUart.Init.StopBits     = UART_STOPBITS_1;
	myUart.Init.Parity       = UART_PARITY_NONE;
	myUart.Init.HwFlowCtl    = UART_HWCONTROL_NONE;
	myUart.Init.Mode         = UART_MODE_TX_RX;
	myUart.Init.OverSampling = UART_OVERSAMPLING_16;

	if(HAL_UART_Init(&myUart) != HAL_OK)
	{
		Error_Handler();
	}
}

void HAL_UART_MspInit(UART_HandleTypeDef *huart)
{
  GPIO_InitTypeDef  GPIO_InitStruct;
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_USART2_CLK_ENABLE();

  GPIO_InitStruct.Pin       = GPIO_PIN_2;
  GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FAST;
  GPIO_InitStruct.Alternate = GPIO_AF7_USART2;

  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  GPIO_InitStruct.Pin = GPIO_PIN_3;
  GPIO_InitStruct.Alternate = GPIO_AF7_USART2;

  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  HAL_NVIC_SetPriority(USART2_IRQn, 0, 1);
  HAL_NVIC_EnableIRQ(USART2_IRQn);
}


void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	uint8_t boton = 0;
	boton = BSP_PB_GetState(BUTTON_KEY);
	if(flag == 0 && boton == PRESSED_BUTTON_USER) {
		flag = 1;
		state = (state + 1) % 2;

		int16_t accBuffer[3];
		__IO int16_t xOffset, yOffset, zOffset;

		/* Leer acelerometro */

		accBuffer[0] = 0;
		accBuffer[1] = 0;
		accBuffer[2] = 0;

		BSP_ACCELERO_GetXYZ(accBuffer);

		xOffset = accBuffer[0];
		yOffset = accBuffer[1];
		zOffset = accBuffer[2];

		// Mandar datos por USB siempre y por Uart dependiendo del enable

		sprintf(bufferSend,"A.USB:%d, %d, %d \r\n", xOffset, yOffset, zOffset);
		len = strlen(bufferSend);

		if (USBD_CDC_SetTxBuffer(&hUSBDDevice, &bufferSend, len)==USBD_OK) {
			if (USBD_CDC_TransmitPacket(&hUSBDDevice)==USBD_OK) {
			}
		}

		sprintf(bufferSend,"A.uart:%d, %d, %d \r\n", xOffset, yOffset, zOffset);
		len = strlen(bufferSend);

		if (flagUart == 1) {
			if(HAL_UART_Transmit(&myUart, (uint8_t*)bufferSend, len, 1000)!= HAL_OK)
			{
				error = 1;
				Error_Handler();
				error = 0;
			}
		}
	}

	else {
		flag = 0;
	}
}

void InitializeTimer()
{
	htim4.Init.Prescaler = 16; //f = 16MHz/16 = 1Mhz (1us)
    htim4.Init.CounterMode = TIM_COUNTERMODE_UP;
    htim4.Init.Period = PERIODO;
    htim4.Init.ClockDivision = 0;
    htim4.Init.RepetitionCounter = 0;
    htim4.Instance = TIM4;
    __TIM4_CLK_ENABLE();
    HAL_TIM_Base_Init(&htim4);
    HAL_TIM_Base_Start(&htim4);
}

void InitializePWMChannel()
{
    sConfigTim4.OCMode = TIM_OCMODE_PWM1;
    sConfigTim4.OCIdleState = TIM_CCx_ENABLE;
    sConfigTim4.OCPolarity = TIM_OCPOLARITY_HIGH;

    sConfigTim4.Pulse = PWM1;
    HAL_TIM_PWM_ConfigChannel(&htim4, &sConfigTim4, TIM_CHANNEL_1);

    sConfigTim4.Pulse = PWM2;
    HAL_TIM_PWM_ConfigChannel(&htim4, &sConfigTim4, TIM_CHANNEL_3);

    sConfigTim4.Pulse = PWM3;
    HAL_TIM_PWM_ConfigChannel(&htim4, &sConfigTim4, TIM_CHANNEL_4);

    HAL_TIM_PWM_Init(&htim4);
    HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_1);
    HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_3);
    HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_4);
}

void leds_PWM()
{
   GPIO_InitTypeDef leds_pwm;
   leds_pwm.Mode = GPIO_MODE_AF_PP;
   leds_pwm.Alternate = GPIO_AF2_TIM4;
   leds_pwm.Pin = GPIO_PIN_12 | GPIO_PIN_14 | GPIO_PIN_15;
   leds_pwm.Pull = GPIO_NOPULL;
   leds_pwm.Speed = GPIO_SPEED_LOW;
   HAL_GPIO_Init(GPIOD,&leds_pwm);

}

void leds_normal()
{

   GPIO_InitTypeDef leds;
   leds.Mode = GPIO_MODE_OUTPUT_PP;
   leds.Alternate = GPIO_AF2_TIM4;
   leds.Pin = GPIO_PIN_12 | GPIO_PIN_14 | GPIO_PIN_15;
   leds.Pull = GPIO_NOPULL;
   leds.Speed = GPIO_SPEED_LOW;
   HAL_GPIO_Init(GPIOD,&leds);
}

void Inicializacion(){
	HAL_Init();
	BSP_PB_Init(BUTTON_KEY, BUTTON_MODE_EXTI);
	BSP_LED_Init(LED3);
	BSP_LED_Init(LED4);
	BSP_LED_Init(LED5);
	BSP_LED_Init(LED6);
}




void HAL_SYSTICK_Callback(void) {
	//Interrupcion cada 1us
	if (t_led4 != 0) {
			t_led4--;
		} else {
			t_led4 = 250;
			BSP_LED_Toggle(LED4);
		}
	if (t_led5 != 0) {
		t_led5--;
		} else {
			t_led5 = 500;
			BSP_LED_Toggle(LED5);
		}
	if (t_led6 != 0) {
		t_led6--;
		} else {
			t_led6 = 1000;
			BSP_LED_Toggle(LED6);
		}
}

void Error_Handler(void)
{
	if (error == 1) {
		sprintf(bufferSend,"No funciona la UART \r\n");
	} else if (error == 2) {
		sprintf(bufferSend,"No funciona el USB \r\n");
	} else {
		sprintf(bufferSend,"No se que falla! \r\n");
	}

	len = strlen(bufferSend);
	if (USBD_CDC_SetTxBuffer(&hUSBDDevice, &bufferSend, len)==USBD_OK) {
		if (USBD_CDC_TransmitPacket(&hUSBDDevice)==USBD_OK) {

		}
	}
  //while(1)
  //{
  //}
}

/**
  * @brief  Tx Transfer completed callback
  * @param  UartHandle: UART handle.
  * @note   This example shows a simple way to report end of IT Tx transfer, and
  *         you can add your own implementation.
  * @retval None
  */
void HAL_UART_TxCpltCallback(UART_HandleTypeDef *UartHandle)
{
  /* Set transmission flag: transfer complete */
  UartReady = SET;

  /* Turn LED6 on: Transfer in transmission process is correct */
  BSP_LED_On(LED6);
}

/**
  * @brief  Rx Transfer completed callback
  * @param  UartHandle: UART handle
  * @note   This example shows a simple way to report end of IT Rx transfer, and
  *         you can add your own implementation.
  * @retval None
  */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *UartHandle)
{
  /* Set transmission flag: transfer complete */
  UartReady = SET;

  /* Turn LED4 on: Transfer in reception process is correct */
  BSP_LED_On(LED4);
}
