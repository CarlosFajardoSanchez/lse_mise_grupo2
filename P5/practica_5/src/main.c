/**
  ******************************************************************************
  * @file    main.c
  * @author  Ac6
  * @version V1.0
  * @date    01-December-2013
  * @brief   Default main function.
  ******************************************************************************
*/

#include <stm32f4xx_hal.h>
#include "stm32f4xx.h"
#include "stm32f411e_discovery.h"

#define PRESSED_BUTTON_NONE 0
#define PRESSED_BUTTON_USER	1


#define PERIODO     1000
#define PWM1        333
#define PWM2        500
#define PWM3        666


TIM_HandleTypeDef htim4;
TIM_OC_InitTypeDef sConfigTim4;

uint8_t state = 0;
uint8_t flag = 0;

int t_led4 = 250;
int t_led5 = 500;
int t_led6 = 1000;

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin);

void InitializeTimer()
{
	htim4.Init.Prescaler = 16; //f = 16MHz/16 = 1Mhz (1us)
    htim4.Init.CounterMode = TIM_COUNTERMODE_UP;
    htim4.Init.Period = PERIODO;
    htim4.Init.ClockDivision = 0;
    htim4.Init.RepetitionCounter = 0;
    htim4.Instance = TIM4;
    __TIM4_CLK_ENABLE();
    HAL_TIM_Base_Init(&htim4);
    HAL_TIM_Base_Start(&htim4);
}

void InitializePWMChannel()
{
    sConfigTim4.OCMode = TIM_OCMODE_PWM1;
    sConfigTim4.OCIdleState = TIM_CCx_ENABLE;
    sConfigTim4.OCPolarity = TIM_OCPOLARITY_HIGH;

    sConfigTim4.Pulse = PWM1;
    HAL_TIM_PWM_ConfigChannel(&htim4, &sConfigTim4, TIM_CHANNEL_1);

    sConfigTim4.Pulse = PWM2;
    HAL_TIM_PWM_ConfigChannel(&htim4, &sConfigTim4, TIM_CHANNEL_3);

    sConfigTim4.Pulse = PWM3;
    HAL_TIM_PWM_ConfigChannel(&htim4, &sConfigTim4, TIM_CHANNEL_4);

    HAL_TIM_PWM_Init(&htim4);
    HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_1);
    HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_3);
    HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_4);
}

void leds_PWM()
{
   GPIO_InitTypeDef leds_pwm;
   leds_pwm.Mode = GPIO_MODE_AF_PP;
   leds_pwm.Alternate = GPIO_AF2_TIM4;
   leds_pwm.Pin = GPIO_PIN_12 | GPIO_PIN_14 | GPIO_PIN_15;
   leds_pwm.Pull = GPIO_NOPULL;
   leds_pwm.Speed = GPIO_SPEED_LOW;
   HAL_GPIO_Init(GPIOD,&leds_pwm);

}

void leds_normal()
{

   GPIO_InitTypeDef leds;
   leds.Mode = GPIO_MODE_OUTPUT_PP;
   leds.Alternate = GPIO_AF2_TIM4;
   leds.Pin = GPIO_PIN_12 | GPIO_PIN_14 | GPIO_PIN_15;
   leds.Pull = GPIO_NOPULL;
   leds.Speed = GPIO_SPEED_LOW;
   HAL_GPIO_Init(GPIOD,&leds);
}

void Inicializacion(){
	HAL_Init();
	BSP_PB_Init(BUTTON_KEY, BUTTON_MODE_EXTI);
	BSP_LED_Init(LED3);
	BSP_LED_Init(LED4);
	BSP_LED_Init(LED5);
	BSP_LED_Init(LED6);
}

int main(void)
{
	Inicializacion();
	InitializeTimer();
	InitializePWMChannel();

	uint16_t contador = 0;

	while(1){

		if (state == 0) {
			leds_normal();
			BSP_LED_Off(LED3);
		}
		else if (state == 1){
			leds_PWM();
			BSP_LED_On(LED3);
		}
	}
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	uint8_t boton = 0;
	boton = BSP_PB_GetState(BUTTON_KEY);
	if(flag == 0 && boton == PRESSED_BUTTON_USER) {
		flag = 1;
		state = (state + 1) % 2;
	}

	else {
		flag = 0;
	}
}



void HAL_SYSTICK_Callback(void) {
	//Interrupcion cada 1us
	if (t_led4 != 0) {
			t_led4--;
		} else {
			t_led4 = 250;
			BSP_LED_Toggle(LED4);
		}
	if (t_led5 != 0) {
		t_led5--;
		} else {
			t_led5 = 500;
			BSP_LED_Toggle(LED5);
		}
	if (t_led6 != 0) {
		t_led6--;
		} else {
			t_led6 = 1000;
			BSP_LED_Toggle(LED6);
		}


}
