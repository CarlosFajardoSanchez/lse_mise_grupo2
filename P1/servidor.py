#!/usr/bin/env python

import select
import socket
import sys
import csv
import datetime

from optparse import OptionParser

parser = OptionParser()
parser.add_option("-r", "--reset_first",
					action="store_true",
					help="reset csv file before launching server")	

(options, args) = parser.parse_args()
if(options.reset_first):
	with open('datos_sensores.csv', 'w') as b:
		writer = csv.writer(b)
		writer.writerow(['id','temperatura','luz','sonido','date'])
	


#cargar el index.html en un buffer: Para mostrar la informacion por web
f = open('index.html', 'r')
index_file_array=f.read()
#print(index_file_array)

host=''
port_sensor= 56000
port_nav= 8080
backlog=5
size=1024

#creo el servidor para atender a los sensores domoticos
server_sensor=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_sensor.bind((host,port_sensor))
server_sensor.listen(backlog)
print("Servidor escuchando a sensores")

#creo el servidor para el navegador:
server_nav=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_nav.bind((host,port_nav))
server_nav.listen(backlog)
print("Servidor escuchando a navegadores")

#meto los servidores y la entrada estandar en la lista input
input=[server_sensor,server_nav,sys.stdin]

running=1
while running:
	print("Entrada en el bucle")
	inputready, outputready,exceptready=select.select(input,[],[])
	
	for s in inputready:

	#si llega un cliente sensor (if s==server_sensor), lo aniado a la lista de eventos a atender
		if s==server_sensor:
			print("Informacion enviada desde sensor domotico")
			client, address=server_sensor.accept()
			input.append(client)

			#si llega un cliente de navegador (if s==server_nav), lo aniado a la lista de eventos a atender
		elif s==server_nav:
			print("Solicitud de un navegador")
			client, address=server_nav.accept()
			input.append(client)

			#si es un evento de teclado o en general de entrada estandar, corto el bucle
		elif s==sys.stdin:
			junk=sys.stdin.readline()
			running=0
	
	#si no es un servidor ni la entrada estandar, es un cliente:
		else:
			print("Entrada en el else previo al recv")
			data=s.recv(size)
			if data: #si hay algo recibido
			
				if "GET" in data: #si es un navegador
					print ("Navegador conectado")
					#client.send("HTTP/1.1 200 OK\n"+"Content-Type: text/html\n"+"\n"+index_file_array);
					
					#imprimir fichero al navegador, reemplazando las palabras clave por sus valores.
					# Read all data from the csv file.
					
					total_number_rows = len(open('datos_sensores.csv').readlines())
					
					with open('datos_sensores.csv', 'rb') as b:
						#Copiamos el template en una variable temporal
						f_table_aux = open('table_aux.html', 'r')
						table_aux = f_table_aux.read()
						new_table = ""
						for i in range(1, total_number_rows) :
							new_table += table_aux.replace('#', str(i))
							
						index_file_array_temp = index_file_array.replace("<!--#Insert_table_here-->", new_table)
						
						reader = csv.reader(b)
						row_number = 0
						for row in reader:
							if row_number == 0:
								header = row
							else:
								col_number = 0
								for col in row:
									if col_number == 0: #ID
										variable = "Node_ID_number_" + str(row_number)
									elif col_number == 1: #Temperatura
										variable = "Temperature_" + str(row_number)
									elif col_number == 2: #Luz
										variable = "Light_" + str(row_number)
									elif col_number == 3: #Sonido
										variable = "Sound_" + str(row_number)
									elif col_number == 4: #Fecha
										variable = "Date_time_" + str(row_number)
									col_number = col_number + 1							
									index_file_array_temp = index_file_array_temp.replace(variable, col)

							row_number = row_number + 1
							
					print(index_file_array_temp)

					client.send("HTTP/1.1 200 OK\n"+"Content-Type: text/html\n"+"\n"+index_file_array_temp);
					s.close()
					input.remove(s)

				#si es un dato de un sensor domotico, guardo el dato en un fichero csv 
				else: 
					#el sensor envia "Nombre_1 Variable_1 Nombre_2 Variable_2 ..."
					print(data)
					tupla = data.split(" ")
					print(tupla)
					
					#Leer cvs para leer los valores por defecto (los que tenia en la tabla)
					temperatura = '--'
					luz = '--'
					sonido = '--'
					
					with open('datos_sensores.csv', 'rb') as b:
						reader = csv.reader(b)
						for row in reader:
							if row[0] == tupla[1]:
								temperatura = row[1]
								luz = row[2]
								sonido = row[3]
										
					for i in range(2, len(tupla)) :
						if (i%2 == 0) :
							nombre = tupla[i]
						else:
							variable_actualizada = tupla[i]
							temperatura = (temperatura, variable_actualizada)[nombre=='temperatura']
							luz = (luz, variable_actualizada)[nombre=='luz']
							sonido = (sonido, variable_actualizada)[nombre=='sonido']

					dateNow = datetime.datetime.now() # Obtenemos el tiempo para actualizarlo
					
					#Cambiar para poder tener cualquier ID

					bottle_list = []
					last_row = 0
					check = False
					
					with open('datos_sensores.csv', 'rb') as b:
						reader = csv.reader(b)
						row_number = 0
						for row in reader:
							if (tupla[1] == row[0]):
								check = True
								datos_actualizar = {row_number:[tupla[1], temperatura, luz, sonido, dateNow] }
							row_number += 1

						#No se ha encontrado el valor recibido en el archivo 
						if (not check):
							last_row = row_number
							datos_actualizar = {last_row:[tupla[1], temperatura, luz, sonido, dateNow] }
						
					
					with open('datos_sensores.csv', 'rb') as b:
						reader = csv.reader(b)	
						bottle_list.extend(reader)
					if (check):
						with open('datos_sensores.csv', 'wb') as b:
							writer = csv.writer(b)
							for line, row in enumerate(bottle_list):
								data = datos_actualizar.get(line, row)
								writer.writerow(data)
					else:
						#No estaba el dato hay que append una nueva fila
						with open('datos_sensores.csv', 'a') as b:
							writer = csv.writer(b)
							data = datos_actualizar.get(last_row)
							writer.writerow(data)
					
			else:
				s.close()
				input.remove(s)
			

